# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2020-11-17)


### Features

* **docs:** add vuepress doc ([c8f1f77](https://gitlab.com/MinaPecheux/rune-vue/commit/c8f1f77083e567036a8c10d7a9ce56008fa2d2e0))
