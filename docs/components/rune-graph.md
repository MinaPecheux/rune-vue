# RuneGraph

The `RuneGraph` is the base component of the lib that allows you to display your graph on a canvas.

## Examples
### Basic

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-basic" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-basic.vue
</SourceCode>
:::

::::

### Dark mode

By default, the `RuneGraph` is in "light" mode. You can easily switch to a dark theme by passing the `color_mode` graph option:

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-dark" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-dark.vue
</SourceCode>
:::

::::

### `RuneGraph` component options

You can pass several direct options to the `RuneGraph` component (see below for more details). For example, you can add a border around the canvas, set the zoom level and set the height:

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-options" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-options.vue
</SourceCode>
:::

::::

### Collapsed mode

Rune nodes, just like in Litegraph.js, can be either in a "normal" or a "collapsed" state. In the collapsed state:

- nodes take less space on the canvas
- inputs and outputs are grouped together in a single dot (if render of collapsed I/O is enabled)
- widgets are disabled (dragging will only drag the node itself)

You can toggle the collapsed on and off by right clicking a node, and you can also pass in the `collapsed` property to your `RuneGraph` component to initially set the collapse state of nodes (false by default):

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-collapsed" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-collapsed.vue
</SourceCode>
:::

::::

### Node shapes

By default, nodes have a box shape (with hard corners). You can define a specific node shape either in the node type constructor itself or directly in your data for a per-node customization. The available shapes are (from left to right, top to bottom):

- `BOX_SHAPE`
- `ROUND_SHAPE`
- `CARD_SHAPE`
- `POINT_SHAPE`

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-shapes" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-shapes.vue
</SourceCode>
:::

::::

### Contextual buttons

Even if a basic `RuneGraph` only emits basic events on select and deselect, you can easily define a set of little contextual buttons to popup around a node on select. These buttons can perform some common tasks (edit the node, remove it from the graph...) and replace the usual full-fledged graph contextual menu (that appears on right-click by default).

For now, you can only place up to 4 buttons on each side of a node.

To use contextual buttons, you need to:

- pass in an array of buttons in the `contextualButtons` prop of your `RuneGraph`, of the form:
  
  ```js
  {
    position: 'top', // or 'bottom', 'left', 'right'
    content: 'Delete',
    callback: (this, node) => {
      this.removeNode(node);
    }
  }
  ```

  or:

  ```js
  {
    position: 'top', // or 'bottom', 'left', 'right'
    content: 'Delete',
    event: 'delete'
  }
  ```

  The content is any valid HTML that will be displayed in the contextual button.

  You can define the action to perform on click either by providing a callback function or the name of an event to emit (that can then be catched in the parent component of your `RuneGraph` component).

  If you use a callback function, you have access to `this` that is the `RuneGraph` Vue component and `node` that is the currently selected node. If you use an event, then simply provide a string: that will be the event emitted by the component whenever the contextual button is clicked.

- optionally, you can also disable the main contextual menu by passing in a graph option (see below for more details):
  
  ```vue
  <rune-graph
    :data="graphData"
    :graph-options="{ show_contextual_menu: false }"
  />
  ```

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-ctx-buttons" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-ctx-buttons.vue
</SourceCode>
:::

::::

### Custom node types and tag

As explained in the [main concepts page](../concepts.md), you can define your own node type and tags pretty quickly with Rune (similarly to Litegraph.js). This demo has a `custom` tag with an `AffineFunction` node type in it. Here is how to load it in a graph:

:::: tabs

::: tab Demo
<Demo component-name="examples-rune-graph-custom-tag" />
:::


::: tab Code
<SourceCode>
<<< @/docs/.vuepress/components/examples/rune-graph-custom-tag.vue
</SourceCode>
:::

::::

## Props

| Property        | Description                      | Type   | Default       |
| --------------- | -------------------------------- | ------ | ------------- |
| `data`          | Graph nodes and edges            | Object | (required)    |
| `layout`        | Nodes layout type                | String | `dagre`       |
| `graphOptions`  | Custom graph options             | Object | `{}`          |
| `layoutOptions` | Custom layout options            | Object | `{}`          |
| `tag`           | Specific tag to load nodes from  | String | `basic`       |
| `tagInitData`   | Additional data that is required to initialize a nodes tag (for example, reference list of items to create node types from)  | Object | `{}`       |
| `autoCenter`     | Whether to automatically recenter nodes in the graph | Boolean | `true`       |
| `collapsed`     | Whether to initially collapse all nodes in the graph | Boolean | `false`       |
| `bordered`     | Whether to add a border around the graph | Boolean | `false`       |
| `height`     | Specific graph height (in pixels) | Number | 400       |
| `zoom`       | Initial graph zoom (a number < 1 will zoom out, a number > 1 will zoom in) | Number | 1       |

## Methods

| Property              | Description
| --------------------- | --------------------------------
| `removeNode(node)`    | Removes the given node from the graph (and tries to auto-reconnect if the option is enabled [default])

## Graph options

The graph and layout options are the same as the ones in the [`rune-core` library](https://minapecheux.gitlab.io/rune-core/api.html#graph-options). You can pass the `graphOptions` and `layoutOptions` properties directly to your `RuneGraph` Vue component.

## Source Code

<SourceCode>
<<< @/src/components/RuneGraph/RuneGraph.vue
</SourceCode>
