---
home: true
heroImage:
actionText: Get Started →
actionLink: /guide
features:
  - title: 🔵 Super-simple data
    details: Create graphs from just a list of nodes and a list of edges :)
  - title: 🟠 Vue JS component
    details: Use a ready-made Vue component to display your graphs with ease!
  - title: 🟣 Styling & customization
    details: Define your own node types and CSS styling to suit your needs
footer: 2020 © Mina Pêcheux - MIT License
---
