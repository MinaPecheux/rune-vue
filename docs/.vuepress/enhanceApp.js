import RuneCore from "@mpecheux/rune-core/dist/rune-core.umd";
import RuneVue from "../../src/plugin.js";

import custom from "./rune-tags/custom.js";

export default ({ Vue /*, options, router, siteData*/ }) => {
  Vue.use(RuneVue, { lib: RuneCore, tags: { custom } });
};
