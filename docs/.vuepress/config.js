module.exports = {
  base: '/rune-vue/',
  dest: 'public',

  locales: {
    '/': {
      lang: 'en-US',
      title: 'Rune Vue 🌀',
      description: 'A Vue JS wrapper around the rune-core JS lib'
    }
  },

  plugins: [ 'vuepress-plugin-element-tabs' ],

  themeConfig: {
    repoLabel: 'Contribute!',
    repo: 'https://gitlab.com/MinaPecheux/rune-vue',
    docsDir: 'docs',
    editLinks: true,
    docsBranch: 'dev',
    editLinkText: 'Help us improve this page!',
    search: false,
    locales: {
      '/': {
        label: 'English',
        selectText: 'Languages',
        lastUpdated: 'Last Updated',
        // service worker is configured but will only register in production
        serviceWorker: {
          updatePopup: {
            message: 'New content is available.',
            buttonText: 'Refresh'
          }
        },
        nav: [
          { text: 'Getting Started', link: '/guide' },
          { text: 'Components', link: '/components/' }
        ],
        sidebar: {
          '/components/': [
            {
              title: 'Components',
              collapsable: false,
              children: ['rune-graph']
            }
          ]
        }
      }
    }
  }
}
