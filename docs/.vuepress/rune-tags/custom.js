import AffineFunction from './affine-function.js'

export default function initialize(context = {}) {
  return {
    'custom/affine-function': AffineFunction
  };
}
