function AffineFunction() {
  this.addInput("x", "number");
  this.addOutput("f", "number");
  const initial_a = 0.5;
  const initial_b = 0;
  this.addProperty("a", initial_a);
  this.addProperty("b", initial_b);
  this.widget = this.addWidget("number", "a", initial_a, "a");
  this.widget = this.addWidget("number", "b", initial_b, "b");
  this.widgets_up = true;
}

AffineFunction.prototype.onExecute = function() {
  let x = this.getInputData(0);
  if (x == null) {
    x = 0;
  }
  const { a, b } = this.properties;
  const f = a * x + b;
  this.setOutputData(0, f);
};

AffineFunction.title = "Affine function";
AffineFunction.desc = "Computes an affine function";

export default AffineFunction;
