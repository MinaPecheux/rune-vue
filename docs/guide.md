# Getting Started

The Rune Vue package is a simple wrapper around the [Rune Core library](https://minapecheux.gitlab.io/rune-core/), a lightweight JS package to draw graphs and flows.

The goal of this specific Vue plugin is to provide a ready-made graph Vue component that can be instantiated with just some nodes and edges data.

For more info on the `RuneGraph` component, check out the [dedicated doc page](./components/rune-graph.md).

## Install the lib

First get the packages:

```sh
# using yarn
yarn add @mpecheux/rune-core
yarn add @mpecheux/rune-vue

# or npm
npm install @mpecheux/rune-core
npm install @mpecheux/rune-vue
```

Then, add them to your Vue app:

```js
import Vue from 'vue';
import RuneCore from "@mpecheux/rune-core/dist/rune-core.umd";
import RuneVue from "@mpecheux/rune-vue";

Vue.use(RuneVue, { lib: RuneCore });
```

## Displaying a basic graph

To create a graph with Rune, you need to:

- define your data: an array of nodes and an array of edges
- instantiate the `RuneGraph` component

Data should be of the following form:

```js
{
  nodes: [
    { id: 'node-0', type: 'basic/input-number' },
    { id: 'node-1', type: 'basic/output' }
  ],
  edges: [
    { source: 'node-0', target: 'node-1' }
  ]
}
```

To create the corresponding graph, simply use the `RuneGraph` Vue component (it is automatically imported in the `Vue.use(...)` step):

```vue
<template>
  <rune-graph :data="graphData" />
</template>

<script>
export default {
  data() {
    return {
      graphData: {
        nodes: [
          { id: 'node-0', type: 'basic/input-number' },
          { id: 'node-1', type: 'basic/output' }
        ],
        edges: [ { source: 'node-0', target: 'node-1' } ]
      }
    };
  }
}
</script>
```

And you're done!

## Author

Rune is a JS library by [Mina Pêcheux](https://www.minapecheux.com). The project started in November 2020.
