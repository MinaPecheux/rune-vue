<template>
  <div ref="container" class="rune-graph" :style="containerStyle">
    <canvas ref="graph" class="rune-graph__canvas" />
    <div
      v-if="contextualButtonsPosition !== null"
      class="rune-graph__contextual-buttons"
      :style="contextualButtonsStyle"
    >
      <rune-graph-contextual-button
        v-for="(button, idx) in contextualButtons"
        :key="idx"
        @click="() => handleContextualButton(idx)"
        :position="contextualButtonsPosition[button.position]"
        :v-html="button.content"
      />
    </div>
  </div>
</template>

<script>
import RuneGraphContextualButton from "../RuneGraphContextualButton";

export default {
  name: "RuneGraph",
  components: { RuneGraphContextualButton },
  props: {
    data: { type: Object, default: () => ({ nodes: [], edges: [] }) },
    layout: { type: String, default: "dagre" },
    graphOptions: { type: Object, default: () => ({}) },
    layoutOptions: { type: Object, default: () => ({}) },
    tags: { type: Array, default: () => [{ name: "basic", context: {} }] },
    tagInitData: { type: Object, default: () => ({}) },
    autoCenter: { type: Boolean, default: true },
    autoExecute: { type: Boolean, default: true },
    collapsed: { type: Boolean, default: false },
    bordered: { type: Boolean, default: false },
    handlesSelection: { type: Boolean, default: true },
    height: { type: Number, default: 200 },
    zoom: { type: Number, default: 1 },
    contextualButtons: { type: Array, default: () => [] }
  },
  data() {
    return {
      graph: null,
      canvas: null,
      nodes: {},
      selectedNodeIndex: null,
      contextualButtonsPosition: null
    };
  },
  mounted() {
    this.$refs.graph.setAttribute("height", this.height);
    for (const { name, context } of this.tags) {
      this.$rune.tags.defineTag(name, context);
    }
    this.setupGraph();
  },
  computed: {
    containerStyle() {
      return {
        border: this.bordered ? "1px solid #222" : null,
        height: `${this.height}px`
      };
    },
    hasContextualButtons() {
      return this.contextualButtons.length > 0;
    },
    contextualButtonsStyle() {
      return {
        left: `${this.contextualButtonsPosition.center[0]}px`,
        top: `${this.contextualButtonsPosition.center[1]}px`
      };
    }
  },
  watch: {
    data() {
      this.setupGraph();
    }
  },
  methods: {
    setupGraph() {
      const graph = new this.$rune.Graph();
      const { width, height } = this.$refs.container.getBoundingClientRect();
      this.$refs.graph.setAttribute("width", width);
      this.$refs.graph.setAttribute("height", height);
      const canvas = new this.$rune.Canvas(
        this.$refs.graph,
        graph,
        this.graphOptions
      );

      const $this = this;
      const nodes = {};
      let i = 0;
      for (let node of this.data.nodes) {
        const n = this.$rune.nodes.createNode(node.type);
        n.id = node.id;
        node.size = n.size;
        if (node.label) {
          n.title = node.label;
        }
        if (node.pos) {
          n.pos = node.pos;
        }
        if (node.shape) {
          n.shape = node.shape;
        }
        if (node.point_size) {
          n.point_size = node.point_size;
        }
        if (node.properties) {
          for (let prop of Object.keys(node.properties)) {
            n.setProperty(prop, node.properties[prop]);
          }
        }
        // use a function to create a closure and use the current
        // index of the for loop in the callback instead of the
        // last one
        // https://stackoverflow.com/questions/21487187/why-is-the-loop-assigning-a-reference-of-the-last-index-element-to
        (function(node, _n, index) {
          if (node.handlesSelection || $this.handlesSelection) {
            _n.onSelected = () => {
              $this.$emit("deselect");
              $this.selectedNodeIndex = index;
              if ($this.hasContextualButtons) {
                $this.placeContextualButtons(_n);
              }
            };
            _n.onDeselected = () => {
              $this.$emit("deselect");
              $this.contextualButtonsPosition = null;
            };
            _n.onMouseMove = () => {
              if (_n.is_selected) {
                $this.placeContextualButtons(_n);
              }
            };
          }
          if (node.onMouseEnter) {
            _n.onMouseEnter = () => node.onMouseEnter(_n);
          }
          if (node.onMouseLeave) {
            _n.onMouseLeave = () => node.onMouseLeave(_n);
          }
          if ($this.autoExecute) {
            _n.onPropertyChanged = () => {
              if (_n.constructor.onPropertyChanged) {
                _n.constructor.onPropertyChanged();
              }
              $this.graph.runStep();
              return false;
            };
          }
        })(node, n, i);
        n.flags = { collapsed: this.collapsed };
        graph.add(n);
        nodes[node.id] = n;
        i++;
      }
      for (let edge of this.data.edges) {
        const { source, target } = edge;
        const source_port = edge.source_port || 0;
        const target_port = edge.target_port || 0;
        nodes[source].connect(source_port, nodes[target], target_port);
      }

      // apply layout to position nodes
      if (this.layout !== "data") {
        let layout;
        switch (this.layout) {
          case "dagre":
            layout = this.$rune.layouts.getDagreLayout(
              this.data,
              this.layoutOptions
            );
            break;
        }
        // (if need be: recompute positions to center graph)
        if (this.autoCenter) {
          layout = this.$rune.layouts.centerLayout(layout, width, height);
        }
        // apply positions
        this.$rune.layouts.applyLayout(graph.getNodes(), layout);
      }

      canvas.onCanvasClick = () => {
        canvas.deselectAllNodes();
      };

      this.graph = graph;
      this.canvas = canvas;
      this.nodes = nodes;
      this.canvas.setZoom(this.zoom);

      if (this.autoExecute) {
        this.graph.runStep();
      }
    },
    placeContextualButtons(node) {
      const { x: cx, y: cy } = node.getCenter(this.canvas);
      const title_height =
        node.node_title_height ||
        node.constructor.node_title_height ||
        this.canvas.node_title_height;
      this.contextualButtonsPosition = {
        center: [cx, cy],
        top: [0, -(node.size[1] + title_height) * 0.5 - 20],
        left: [-node.size[0] * 0.5 - 20, 0],
        right: [node.size[0] * 0.5 + 20, 0],
        bottom: [0, (node.size[1] + title_height) * 0.5 + 20]
      };
    },
    handleContextualButton(index) {
      const btn = this.contextualButtons[index];
      const nodeId = this.data.nodes[this.selectedNodeIndex].id;
      const node = this.nodes[nodeId];
      if (btn.callback) {
        btn.callback(this, node);
      } else if (btn.event) {
        this.$emit(btn.event, { node });
      }
      this.contextualButtonsPosition = null;
    },
    removeNode(node = null) {
      if (node === null) {
        const nodeId = this.data.nodes[this.selectedNodeIndex].id;
        node = this.nodes[nodeId];
      }
      this.graph.remove(node);
      this.$emit("deselect");
      this.selectedNodeIndex = null;
    }
  }
};
</script>

<style>
.rune-graph {
  position: relative;
  width: 100%;
  margin-top: var(--gutter);
}

.rune-graph__canvas {
  display: block;
  border: none;
}

.rune-graph__contextual-buttons {
  position: absolute;
  width: 100px;
  height: 100px;
}
</style>
