import * as components from "./components";

const RuneGraphPlugin = {
  install(Vue, options = {}) {
    if (this.installed) {
      return;
    }

    this.installed = true;

    for (const componentName in components) {
      const component = components[componentName];
      Vue.component(component.name, component);
    }

    const { lib } = options;

    if (!options.no_basic) {
      lib.tags.registerTag("basic", lib.basic);
      // add basic tag to current list
      lib.tags.defineTag("basic");
    }

    // register additional tags, if any
    const tags = options.tags || {};
    for (const tag of Object.keys(tags)) {
      lib.tags.registerTag(tag, tags[tag]);
    }

    Vue.prototype.$rune = lib;
  }
};

if (typeof window !== "undefined" && window.Vue) {
  window.Vue.use(RuneGraphPlugin);
}

export default RuneGraphPlugin;
